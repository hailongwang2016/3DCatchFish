﻿/****************************************************************
*   作者：corer
*   创建时间：2018/2/9 19:14:12
*   描述说明：
*****************************************************************/

using System;

namespace Hotfix
{
  /// <summary>
  /// 组件工场
  /// </summary>
  public static class ComponentFactory
  {
    /// <summary>
    /// 创建组件
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="entity"></param>
    /// <returns></returns>
    public static T Create<T>(Component entity) where T : Component, new()
    {
      T t = Game.ObjectPool.Fetch<T>();
      t.Parent = entity;
      Game.EventSystem.Awake(t);
      return t;
    }

    public static T Create<T, P>(Component entity, P p) where T : Component, new()
    {
      T t = Game.ObjectPool.Fetch<T>();
      t.Parent = entity;
      Game.EventSystem.Awake(t, p);
      return t;
    }

    public static T Create<T, P>(P p) where T : Component, new()
    {
      T t = Game.ObjectPool.Fetch<T>();
      Game.EventSystem.Awake(t, p);
      return t;
    }

    public static T Create<T, P1, P2>(Component entity, P1 p1, P2 p2) where T : Component, new()
    {
      T t = Game.ObjectPool.Fetch<T>();
      t.Parent = entity;
      Game.EventSystem.Awake(t, p1, p2);
      return t;
    }

    public static T CreateWithId<T>(long id) where T : Component
    {
      T disposer = Game.ObjectPool.Fetch<T>();
      disposer.Id = id;
      Game.EventSystem.Awake(disposer);
      return disposer;
    }

    public static T CreateWithId<T, A>(long id, A a) where T : Component
    {
      T disposer = Game.ObjectPool.Fetch<T>();
      disposer.Id = id;
      Game.EventSystem.Awake(disposer, a);
      return disposer;
    }

    public static T CreateWithId<T, A, B>(long id, A a, B b) where T : Component
    {
      T disposer = Game.ObjectPool.Fetch<T>();
      disposer.Id = id;
      Game.EventSystem.Awake(disposer, a, b);
      return disposer;
    }
  }
}