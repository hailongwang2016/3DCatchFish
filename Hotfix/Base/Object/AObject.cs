﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/8 21:59:20
*   描述说明：
*****************************************************************/
using ETSModel;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel;

namespace Hotfix
{

  public interface IDisposable2
  {
    void Dispose();
  }

  /// <summary>
  /// 顶层类
  /// </summary>
  public abstract class AObject : IDisposable2
  {
    /// <summary>
    /// ID
    /// </summary>

    public long Id { get; set; }
    /// <summary>
    /// 是否来之池子中
    /// </summary>
    public bool IsFromPool { get; set; }

    public AObject()
    {
      this.Id = IdGenerater.GeneraterId();
    }

    public AObject(long id)
    {
      this.Id = id;
    }

    public virtual void Dispose()
    {
      if (this.Id == 0)
        return;

      this.Id = 0;

      if (IsFromPool) // 从池子中来，释放时，就回到池子中。
      {
        Game.ObjectPool.Recycle(this);
      }
    }
  }
}