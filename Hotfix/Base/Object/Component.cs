﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/8 22:03:00
*   描述说明：
*****************************************************************/

namespace Hotfix
{
  /// <summary>
  /// 组件
  /// </summary>
  public abstract class Component : AObject
  {
    /// <summary>
    /// 组件挂载的Entity
    /// </summary>
    public AObject Parent { get; set; }

    public Entity Entity
    {
      get
      {
        return this.Parent as Entity;
      }
    }

    public T GetParent<T>() where T : AObject
    {
      return (T)this.Parent;
    }

    protected Component()
    {
      this.Id = 1;
    }

    public override void Dispose()
    {
      if (this.Id == 0)
      {
        return;
      }
      base.Dispose();
    }
  }
}