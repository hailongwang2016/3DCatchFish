﻿using ETSModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotfix
{
  public class Init
  {
    public static void Start()
    {
      ETSModel.Game.Hotfix.Update = () => { Update(); };

      Log.Info("in hotfix ...");
    }

    public static void Update()
    {
      Game.EventSystem.Update();
    }
  }
}