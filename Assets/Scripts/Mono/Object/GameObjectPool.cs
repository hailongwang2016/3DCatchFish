﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/9 11:59:51
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Object = UnityEngine.Object;
namespace ETSModel
{
  /// <summary>
  /// Unity对象池
  /// </summary>
  public class GameObjectPool
  {
    private Dictionary<Type, Queue<Object>> _cache = new Dictionary<Type, Queue<Object>>();

    public Object Fetch(Type type)
    {
      Queue<Object> queue;
      if (_cache.TryGetValue(type, out queue))
      {
        queue = new Queue<Object>();
        _cache.Add(type, queue);
      }

      Object obj;
      if (queue.Count > 0)
      {
        obj = queue.Dequeue();
        return obj;
      }

      obj = (Object)Activator.CreateInstance(type);
      return obj;
    }

    public T Fetch<T>() where T : Object
    {
      return (T)this.Fetch(typeof(T));
    }

    public void Recycle(Object obj)
    {
      Type type = obj.GetType();
      Queue<Object> queue;
      if (!_cache.TryGetValue(type, out queue))
      {
        queue = new Queue<Object>();
        this._cache.Add(type, queue);
      }
      queue.Enqueue(obj);
    }
  }
}