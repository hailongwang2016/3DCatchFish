﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/6 15:21:04
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
  public class ViewAttribute : Attribute
  {
    public string Key { get; set; }
  }
}