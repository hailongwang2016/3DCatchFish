﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 15:03:49
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETSModel
{
  public interface IUIFactory
  {
    UI Create(UIType type, GameObject parent);
    void Remove(UIType type);
  }
}
