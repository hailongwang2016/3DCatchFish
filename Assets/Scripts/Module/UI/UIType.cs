﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 15:02:54
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public enum UIType
  {
    Root = 0,
    UILauncher,
    UILoading,
    UIReady,
    UIGame,
    UIResult
  }
}