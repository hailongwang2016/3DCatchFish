﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 15:04:56
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
  public class UIFactoryAttribute : Attribute
  {
    public int Type { get; set; }
    public UIFactoryAttribute(UIType type)
    {
      this.Type = (int)type;
    }
  }
}