﻿using UnityEngine;

namespace ETSModel
{
  /// <summary>
  /// 窗口层级
  /// </summary>
  public enum WindowLayer
  {
    UIHiden = 0, // 隐藏层
    Bottom,       // 最低层
    Medium,       // 中间层，一般窗口都在这层。
    Top,          // 弹出层
    TopMost       // 遮罩层
  }

  public class UIConfig : MonoBehaviour
  {
    public WindowLayer WindowLayer = WindowLayer.Medium;
  }
}