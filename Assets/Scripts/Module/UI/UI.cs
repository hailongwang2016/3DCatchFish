﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 14:19:53
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETSModel
{
  [ObjectSystem]
  public class UISystem : ObjectSystem<UI>, IAwake<GameObject>
  {
    public void Awake(GameObject p)
    {
      Self.Awake(p);
    }
  }

  public class UI : Entity
  {
    public Scene Scene { get; private set; }

    public GameObject GameObject { get; private set; }

    public string Name
    {
      get
      {
        return this.GameObject.name;
      }
    }

    public void Awake(GameObject gameObject)
    {
      this.GameObject = gameObject;
    }

    public override void Dispose()
    {
      if (this.Id == 0)
        return;

      base.Dispose();

      UnityEngine.Object.Destroy(this.GameObject);
    }

    /// <summary>
    /// 在同级中的第一个位置
    /// </summary>
    public void SetAsFirstSibling()
    {
      this.GameObject.transform.SetAsFirstSibling();
    }
  }
}