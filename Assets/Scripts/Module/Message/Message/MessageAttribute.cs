﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/25 14:56:56
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  public class MessageAttribute : Attribute
  {
    public ushort Opcode { get; }

    public MessageAttribute(ushort opcode)
    {
      this.Opcode = opcode;
    }
  }
}
