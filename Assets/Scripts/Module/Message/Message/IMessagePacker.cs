﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/23 11:31:51
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  public interface IMessagePacker
  {
    byte[] Serialize(object obj);

    T DeserializeFrom<T>(byte[] bytes);
    T DeserializeFrom<T>(byte[] bytes, int index, int count);

    object DeserializeFrom(Type type, byte[] bytes);
    object DeserializeFrom(Type type, byte[] bytes, int index, int count);
  }
}