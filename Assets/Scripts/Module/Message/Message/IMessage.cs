﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/22 21:31:40
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using ProtoBuf;
namespace ETSModel
{

  public struct PacketInfo
  {
    public ushort Opcode;
    public uint RpcId;
    public byte[] Bytes;
    public ushort Index;
    public ushort Length;
  }

  [ProtoContract]
  public partial class MessageObject
  {

  }

  public interface IMessage
  {

  }

  public interface IRequest : IMessage
  {

  }

  public interface IResponse : IMessage
  {
    int Error { get; set; }
    string Message { get; set; }
  }
}
