﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/23 10:52:29
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  public interface IMessageDispatcher
  {
    void Dispatch(Session session, PacketInfo packetInfo);
  }
}
