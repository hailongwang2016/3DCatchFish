﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/26 15:44:54
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  public interface IMHandler
  {
    void Handle(Session session, uint rpcId, IMessage message);
    Type GetMessageType();
  }
}
