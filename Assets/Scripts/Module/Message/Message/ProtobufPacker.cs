﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/23 19:49:25
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  public class ProtobufPacker : IMessagePacker
  {
    public T DeserializeFrom<T>(byte[] bytes)
    {
      return ProtobufHelper.FromBytes<T>(bytes);
    }

    public T DeserializeFrom<T>(byte[] bytes, int index, int count)
    {
      return ProtobufHelper.FromBytes<T>(bytes, index, count);
    }

    public object DeserializeFrom(Type type, byte[] bytes)
    {
      return ProtobufHelper.FromBytes(type, bytes);
    }

    public object DeserializeFrom(Type type, byte[] bytes, int index, int count)
    {
      return ProtobufHelper.FromBytes(type, bytes, index, count);
    }

    public byte[] Serialize(object obj)
    {
      return ProtobufHelper.ToBytes(obj);
    }
  }
}
