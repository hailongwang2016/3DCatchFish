﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/26 15:55:47
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  public class MessageHandlerAttribute : Attribute
  {
    public AppType Type { get; }

    public MessageHandlerAttribute(AppType appType)
    {
      this.Type = appType;
    }
  }
}