﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 10:31:18
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public class ClientMessageDispatcher : IMessageDispatcher
  {
    // 热更新消息处理回调
    public Action<Session, PacketInfo> HotfixCallback;

    public void Dispatch(Session session, PacketInfo packetInfo)
    {
      HotfixCallback?.Invoke(session, packetInfo);

      Type messageType = Game.Scene.GetComponent<OpcodeTypeComponent>().GetType(packetInfo.Opcode);
      IMessage message = (IMessage)session.Network.MessagePacker.DeserializeFrom(messageType, packetInfo.Bytes, packetInfo.Index, packetInfo.Length);

      Game.Scene.GetComponent<MessageDispatcherComponent>().Handle(session, packetInfo.RpcId, message);
    }
  }
}