﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/26 16:25:44
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  public static class ErrorCode
  {
    // 框架内部错误
    public const int ERR_RpcFail = 101;
  }
}
