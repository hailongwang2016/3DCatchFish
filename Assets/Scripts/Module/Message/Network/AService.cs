﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/10 22:42:33
*   描述说明：
*****************************************************************/

using System;
using System.Net;
using System.Threading.Tasks;

namespace ETSModel
{

  public enum NetworkProtocol
  {
    TCP,
    KCP,
  }

  /// <summary>
  /// 一种服务
  /// </summary>
  public abstract class AService : IDisposable
  {
    /// <summary>
    /// 获取一个连接
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public abstract AChannel GetChannel(long id);
    /// <summary>
    /// 接受一个连接
    /// </summary>
    /// <returns></returns>
    public abstract Task<AChannel> AcceptChannel();

    /// <summary>
    /// 连接
    /// </summary>
    /// <param name="iPEndPoint"></param>
    /// <returns></returns>
    public abstract AChannel ConnectChannel(IPEndPoint iPEndPoint);

    /// <summary>
    /// 移除一个连接
    /// </summary>
    /// <param name="channelId"></param>
    public abstract void Remove(long channelId);

    /// <summary>
    /// 给KCP用的
    /// </summary>
    public virtual void Update() { }

    /// <summary>
    /// 释放资源
    /// </summary>
    public abstract void Dispose();
  }
}