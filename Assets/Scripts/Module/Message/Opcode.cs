﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/25 14:54:06
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  public static partial class Opcode
  {
    public const ushort C2G_LONG = 1001;
    public const ushort G2C_LONG = 1002;

    public const ushort G2L_Ping = 1003;
    public const ushort L2G_Ping = 1004;
  }
}