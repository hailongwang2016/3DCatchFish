﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 9:59:54
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  [ObjectSystem]
  public class ClientNetworkComponentSystem : ObjectSystem<ClientNetworkComponent>, IAwake
  {
    public void Awake()
    {
      Self.Awake();
    }
  }

  public class ClientNetworkComponent : NetworkComponent
  {
    public static Session Session { get; set; }

    public void Awake()
    {
      this.Awake(NetworkProtocol.TCP);
      this.MessagePacker = new ProtobufPacker();
    }
  }
}