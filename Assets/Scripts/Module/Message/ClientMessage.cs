﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/25 14:54:32
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using ProtoBuf;
namespace ETSModel
{
  [Message(Opcode.C2G_LONG)]
  [ProtoContract]
  public partial class C2G_Login : IRequest
  {
    [ProtoMember(1)]
    public string UserName;
    [ProtoMember(2)]
    public string Password;
  }

  [Message(Opcode.G2C_LONG)]
  [ProtoContract]
  public partial class G2C_Login : IResponse
  {
    [ProtoMember(1)]
    public string UserName;
    [ProtoMember(2)]
    public string Password;

    [ProtoMember(90)]
    public int Error { get; set; }
    [ProtoMember(91)]
    public string Message { get; set; }
  }

}