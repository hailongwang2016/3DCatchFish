﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/24 20:07:51
*   描述说明：
*****************************************************************/
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  /// <summary>
  /// 每个Config的基类
  /// </summary>
  public abstract class AConfigComponent : Component, ISerializeToEntity
  {

  }
}