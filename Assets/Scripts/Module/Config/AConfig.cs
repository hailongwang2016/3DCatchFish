﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/24 20:02:58
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ETSModel
{
  public abstract class AConfig : Entity
  {
    protected AConfig()
    {
    }

    protected AConfig(long id) : base(id)
    {
    }
  }
}
