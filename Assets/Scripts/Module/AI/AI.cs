﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/9 13:35:46
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;
namespace ETSModel
{

  public enum AIState
  {
    Move,
    Attack,
    Death
  }

  public class AI : MonoBehaviour
  {

    public AIState aIState = AIState.Move;

    public int score = 100;

    // 目标点
    public Transform _point;

    // 移动速度
    public float moveSpeed;

    // 攻击距离
    public float attackDistance;

    // 是否已经攻击过
    public bool isAttacked = false;

    [HideInInspector]
    public Rigidbody rig;

    [HideInInspector]
    public Animator ani;

    // 是否是Boss
    public bool isBoss = false;

    // boss的血
    public int Hp;

    private void Awake()
    {
      this.rig = GetComponent<Rigidbody>();
      this.ani = GetComponent<Animator>();

      this._point = Camera.main.transform;

      if (isBoss)
      {
        //this.moveSpeed /= 2; // 速度减半
        Game.Scene.GetComponent<UIComponent>().Get(UIType.UIGame).GetComponent<UIGameComponent>().ShowHp();
      }
    }

    public void Update()
    {
      if (this.aIState == AIState.Death) return;

      switch (this.aIState)
      {
        case AIState.Move:
          Move();
          break;
        case AIState.Attack:
          Attack();
          break;
        default:
          break;
      }
    }

    public void Move()
    {
      rig.velocity = transform.forward * this.moveSpeed * Time.deltaTime;

      if (this.isAttacked) return;

      var target = new Vector2(_point.position.x, _point.position.z);
      var self = new Vector2(transform.position.x, transform.position.z);
      if (Vector2.Distance(target, self) <= attackDistance && this.isAttacked != true) // 在攻击范围内，并且没有攻击过。
      {
        this.aIState = AIState.Attack;
        this.isAttacked = true;
        ani.SetTrigger("attack");
        Attack();
      }
    }

    System.Random random = new System.Random();
    public void Attack()
    {
      int i = random.Next(100);
      if (i == 1)
      {
        Camera.main.transform.DOShakePosition(0.4f, strength: 0.025f);
      }

      // 如果是Boss，就缩小到原来大小。
      if (this.isBoss)
      {
        transform.DOScale(1f, 1f);
      }
    }

    /// <summary>
    /// 收到伤害
    /// </summary>
    public virtual void OnDamage()
    {
      if (this.aIState == AIState.Death) return;

      if (this.isBoss)
      {
        this.Hp -= 100;

        if (this.Hp <= 0)
        {
          Deatch();
          Game.Scene.GetComponent<UIComponent>().Get(UIType.UIGame).GetComponent<UIGameComponent>().HideHp();
        }
        return;
      }

      Deatch();
    }


    public async void Deatch()
    {
      ani.SetBool("death", true);

      this.aIState = AIState.Death;

      await Task.Delay(1000);

      transform.DOLocalMoveY(transform.position.y - 1, 1f).OnComplete(() =>
      {
        Destroy(gameObject);
      });
    }

    private void OnTriggerEnter(Collider other)
    {
      if (other.name == "Destory")
      {
        Destroy(gameObject);
      }
    }
  }
}