﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/3 16:08:20
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public interface IFSMState<T> where T : class
  {
    void Init(T ai);
    void Enter(T ai);
    void Exit(T ai);
    void Update(T ai);
    Type CheckTransition(T ai); // 
  }
}