﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/5 15:45:15
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  [Event(EventIdType.LoadingEnd)]
  public class LoadingEndEvent_UILoading : AEvent
  {
    public async override void Run()
    {
      Game.Scene.GetComponent<UIComponent>().Create(UIType.UILoading);
      await Task.Delay(3000);
      await Game.Scene.GetComponent<SceneChangeComponent>().ChangeSceneAsync(SceneType.Game);
      Game.EventSystem.Run(EventIdType.StartGame);
      Game.Scene.GetComponent<UIComponent>().Remove(UIType.UILoading);
    }
  }
}