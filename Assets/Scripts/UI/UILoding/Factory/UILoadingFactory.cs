﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/5 15:40:06
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETSModel
{
  [UIFactory(UIType.UILoading)]
  public class UILoadingFactory : IUIFactory
  {
    public UI Create(UIType type, GameObject parent)
    {
      GameObject original = Resources.Load<GameObject>("UI/UILoading");
      GameObject go = UnityEngine.Object.Instantiate(original, parent.transform);
      UI ui = ComponentFactory.CreateWithId<UI, GameObject>(IdGenerater.GeneraterId(), go);
      return ui;
    }

    public void Remove(UIType type)
    {

    }
  }
}