﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 17:38:02
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETSModel
{
  [UIFactory(UIType.UILauncher)]
  public class UILauncherFactory : IUIFactory
  {
    public UI Create(UIType type, GameObject parent)
    {
      GameObject original = Resources.Load<GameObject>("UI/UILauncher");
      GameObject login = GameObject.Instantiate(original);
      UI ui = ComponentFactory.CreateWithId<UI, GameObject>(IdGenerater.GeneraterId(), login);
      ui.AddComponent<UILauncherComponent>();
      return ui;
    }

    public void Remove(UIType type)
    {
      Game.Scene.GetComponent<UIComponent>().Get(type).Dispose();
    }
  }
}