﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/1 11:37:10
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ETSModel
{
  [ObjectSystem]
  public class UILauncherComponentSystem : ObjectSystem<UILauncherComponent>, IAwake
  {
    public void Awake()
    {
      Self.Awake();
    }
  }

  public class UILauncherComponent : Component
  {
    [View]
    public Button Button { get; set; }

    public void Awake()
    {
      Button = this.GetParent<UI>().GameObject.Get<GameObject>("BtnGameOne").GetComponent<Button>();

      Button.onClick.AddListener(OpenGame);
    }

    public void OpenGame()
    {

    }
  }
}