﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/7 14:15:09
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  [Event(EventIdType.StartGame)]
  public class StartGameEvent_UIGame : AEvent
  {
    public override void Run()
    {
      Game.Scene.GetComponent<UIComponent>().Create(UIType.UIGame);
    }
  }
}