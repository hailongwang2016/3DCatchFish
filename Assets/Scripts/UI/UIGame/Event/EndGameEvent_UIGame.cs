﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/9 19:03:40
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETSModel
{
  [Event(EventIdType.EndGame)]
  public class EndGameEvent_UIGame : AEvent
  {
    public override void Run()
    {
      Time.timeScale = 0;
      Game.Scene.GetComponent<UIComponent>().Create(UIType.UIResult);
    }
  }
}