﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/7 14:16:32
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETSModel
{
  [UIFactory(UIType.UIResult)]
  public class UIResultFactory : IUIFactory
  {
    public UI Create(UIType type, GameObject parent)
    {
      var origianl = Resources.Load<GameObject>("UI/UIResult");
      var uiGame = UnityEngine.Object.Instantiate(origianl);

      UI ui = ComponentFactory.CreateWithId<UI, GameObject>(IdGenerater.GeneraterId(), uiGame);
      ui.AddComponent<UIResultComponent>();

      return ui;
    }

    public void Remove(UIType type)
    {

    }
  }
}