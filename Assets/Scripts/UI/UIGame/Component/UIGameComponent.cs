﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/7 16:50:22
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;

namespace ETSModel
{
  [ObjectSystem]
  public class UIGameComponentSystem : ObjectSystem<UIGameComponent>, IAwake, IUpdate
  {
    public void Awake()
    {
      Self.Awake();
    }

    public void Update()
    {
      Self.Update();
    }
  }

  public class UIGameComponent : Component
  {
    [View]
    public Text Timer { get; set; }
    [View]
    public Text Score { get; set; }
    [View]
    public Image Hp { get; set; }

    public void Awake()
    {
      GetParent<UI>().InjectView(this);
    }

    public void Update()
    {
      //Score.text = Game.Scene.GetComponent<GamerComponent>()?.Score.ToString();
      //Timer.text = $"{Game.Scene.GetComponent<GamerComponent>()?.Time.ToString()}";
    }

    /// <summary>
    /// 显示HpUI
    /// </summary>
    public void ShowHp()
    {
      Hp.gameObject.SetActive(true);
    }

    /// <summary>
    /// 隐藏HpUI
    /// </summary>
    public void HideHp()
    {
      Hp.gameObject.SetActive(false);
    }

    /// <summary>
    /// 更新Hp
    /// </summary>
    public void UpdateHp(int value)
    {
      this.Hp.fillAmount = value;
    }
  }
}