﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/9 19:06:24
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;

namespace ETSModel
{
  [ObjectSystem]
  public class UIResultComponentSystem : ObjectSystem<UIResultComponent>, IAwake
  {
    public void Awake()
    {
      Self.Awake();
    }
  }

  public class UIResultComponent : Component
  {
    [View]
    public Text Score { get; set; }
    [View]
    public Text Text { get; set; }
    public void Awake()
    {
      GetParent<UI>().InjectView(this);

      //Score.text = Game.Scene.GetComponent<GamerComponent>().Score.ToString();
    }
  }
}