﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 15:51:23
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public static partial class EventIdType
  {
    // 开始加载
    public const int LoadingBegin = 1000;
    public const int LoadingEnd = 1001;

    public const int StartGame = 2001;
    public const int EndGame = 2002;
  }
}