﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 15:45:44
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public interface IEvent
  {
    void Handle();
    void Handle(object a);
  }

  public abstract class AEvent : IEvent
  {
    public void Handle()
    {
      this.Run();
    }

    public void Handle(object a)
    {
      throw new NotImplementedException();
    }

    public abstract void Run();
  }

  public abstract class AEvent<T> : IEvent
  {
    public void Handle()
    {
      throw new NotImplementedException();
    }

    public void Handle(object a)
    {
      this.Run((T)a);
    }

    public abstract void Run(T obj);
  }
}
