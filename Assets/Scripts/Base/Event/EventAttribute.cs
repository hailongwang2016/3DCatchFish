﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 15:45:58
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
  public class EventAttribute : Attribute
  {
    public int EventId { get; private set; }
    public EventAttribute(int eventId)
    {
      this.EventId = eventId;
    }
  }
}