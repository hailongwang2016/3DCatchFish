﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 9:53:12
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
  public class ILAdapterAttribute : Attribute
  {
  }
}
