﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/26 22:21:10
*   描述说明：
*****************************************************************/
namespace ETSModel
{
  public static class Log
  {
    public static void Trace(string message)
    {
      UnityEngine.Debug.Log(message);
    }

    public static void Info(string message)
    {
      UnityEngine.Debug.Log(message);
    }

    public static void Debug(string message)
    {
      UnityEngine.Debug.LogWarning(message);
    }

    public static void Error(string message)
    {
      UnityEngine.Debug.LogError(message);
    }
  }
}