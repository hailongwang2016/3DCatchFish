﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/9 18:42:52
*   描述说明：
*****************************************************************/

using System;

namespace ETSModel
{
  /// <summary>
  /// 时间帮助类
  /// </summary>
  public static class TimeHelper
  {
    // 
    public static readonly long epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;

    /// <summary>
    /// 客户端当前时间
    /// </summary>
    /// <returns></returns>
    public static long ClientNow()
    {
      return (DateTime.UtcNow.Ticks - epoch) / 10000;
    }

    /// <summary>
    /// 客户端当前的秒数
    /// </summary>
    /// <returns></returns>
    public static long ClientNowSeconds()
    {
      // 10000000 tick 相当于 1秒
      return (DateTime.UtcNow.Ticks - epoch) / 10000000;
    }
  }
}