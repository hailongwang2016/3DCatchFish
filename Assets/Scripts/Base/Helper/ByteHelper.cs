﻿/****************************************************************
*   作者：qq918
*   创建时间：2017/12/20 13:53:09
*   描述说明：
*   Byte数组帮助类
*****************************************************************/
using System.Text;

namespace ETSModel
{
  public static class ByteHelper
  {
    /// <summary>
    /// 转换到16进制文本
    /// </summary>
    /// <param name="bytes"></param>
    /// <param name="format"></param>
    /// <returns></returns>
    public static string ToHex(this byte[] bytes)
    {
      StringBuilder stringBuilder = new StringBuilder();

      foreach (var b in bytes)
      {
        stringBuilder.Append(b.ToString("X2"));
      }
      return stringBuilder.ToString();
    }

    /// <summary>
    /// 转换到16进制文本
    /// </summary>
    /// <param name="bytes"></param>
    /// <param name="format"></param>
    /// <returns></returns>
    public static string ToHex2(this byte[] bytes)
    {
      StringBuilder stringBuilder = new StringBuilder();

      for (int i = 0; i < bytes.Length; i++)
      {
        var b = bytes[i];
        stringBuilder.Append(b.ToString("X2"));
        if (i != bytes.Length - 1)
          stringBuilder.Append(" ");
      }
      return stringBuilder.ToString();
    }
  }
}