﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/9 21:47:22
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ETSModel
{
  /// <summary>
  /// Dll加载
  /// </summary>
  public static class DllHelper
  {
    public static Assembly LoadModel()
    {
      return typeof(Game).Assembly;
    }

#if SERVER
    /// <summary>
    /// 加载热更新代码
    /// </summary>
    /// <returns></returns>
    public static Assembly LoadHotfix()
    {
      string basePath = AppDomain.CurrentDomain.BaseDirectory;

      byte[] dll = File.ReadAllBytes(basePath + "Hotfix.dll");
      byte[] pdb = File.ReadAllBytes(basePath + "Hotfix.pdb");

      return Assembly.Load(dll, pdb);
    }
#endif

    public static Type[] GetAll()
    {
      List<Type> types = new List<Type>();
      foreach (Assembly item in Game.EventSystem.GetAllAssembly())
      {
        types.AddRange(item.GetTypes());
      }
      return types.ToArray();
    }
  }
}
