﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/6 11:47:31
*   描述说明：
*****************************************************************/

using LitJson;
using System;
using System.ComponentModel;

namespace ETSModel
{
  public static class JsonHelper
  {
    public static string ToJson(object obj)
    {
      return JsonMapper.ToJson(obj);
    }

    public static T FromJson<T>(string str)
    {
      T t = JsonMapper.ToObject<T>(str);
      ISupportInitialize iSupportInitialize = t as ISupportInitialize;
      if (iSupportInitialize == null)
      {
        return t;
      }
      iSupportInitialize.EndInit();
      return t;
    }

    public static object FromJson(Type type, string str)
    {
      object t = JsonMapper.ToObject(type, str);
      ISupportInitialize iSupportInitialize = t as ISupportInitialize;
      if (iSupportInitialize == null)
      {
        return t;
      }
      iSupportInitialize.EndInit();
      return t;
    }

    public static T Clone<T>(T t)
    {
      return FromJson<T>(ToJson(t));
    }
  }
}
