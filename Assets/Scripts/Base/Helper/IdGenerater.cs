﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/9 18:55:57
*   描述说明：
*****************************************************************/
namespace ETSModel
{
  /// <summary>
  /// Id生成类
  /// </summary>
  public static class IdGenerater
  {
    /// <summary>
    /// 应用ID
    /// </summary>
    public static long AppId { private get; set; }
    /// <summary>
    /// 值
    /// </summary>
    private static ushort value;
    /// <summary>
    /// 生成ID
    /// </summary>
    /// <returns></returns>
    public static long GeneraterId()
    {
      long time = TimeHelper.ClientNowSeconds();
      return (AppId << 48) + (time << 16) + ++value;
    }
  }
}