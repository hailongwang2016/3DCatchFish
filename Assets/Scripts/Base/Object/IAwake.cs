﻿/****************************************************************
*   作者：corer
*   创建时间：2018/2/9 19:28:07
*   描述说明：
*****************************************************************/

namespace ETSModel
{
  public interface IAwake
  {
    void Awake();
  }

  public interface IAwake<P>
  {
    void Awake(P p);
  }

  public interface IAwake<P1, P2>
  {
    void Awake(P1 p1, P2 p2);
  }
}