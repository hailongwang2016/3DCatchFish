﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/6 11:55:48
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ETSModel
{
  public class OneThreadSynchronizationContext : SynchronizationContext
  {
    // 线程同步队列,发送接收socket回调都放到该队列,由poll线程统一执行
    private readonly ConcurrentQueue<Action> queue = new ConcurrentQueue<Action>();

    private void Add(Action action)
    {
      this.queue.Enqueue(action);
    }

    public void Update()
    {
      while (true)
      {
        Action a;
        if (!this.queue.TryDequeue(out a))
        {
          return;
        }
        a();
      }
    }

    public override void Post(SendOrPostCallback callback, object state)
    {
      this.Add(() => { callback(state); });
    }
  }
}
