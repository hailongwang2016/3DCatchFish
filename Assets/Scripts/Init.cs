﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

namespace ETSModel
{
  public class Init : MonoBehaviour
  {
    public TextAsset GlobalProto;

    private readonly OneThreadSynchronizationContext context = new OneThreadSynchronizationContext();

    private void Awake()
    {
      DontDestroyOnLoad(this);
    }

    private void Start()
    {
      SynchronizationContext.SetSynchronizationContext(this.context);

      Game.EventSystem.Add(AssemblyType.Model, DllHelper.LoadModel());

      Game.Scene.AddComponent<GlobalConfigComponent, string>(GlobalProto.text);
      Game.Scene.AddComponent<UnitComponent>();
      Game.Scene.AddComponent<PlayerComponent>();
      Game.Scene.AddComponent<OpcodeTypeComponent>();
      Game.Scene.AddComponent<UIComponent>();

      Game.Hotfix.LoadHotfixAssembly();

      Game.EventSystem.Run(EventIdType.LoadingBegin);

      Game.Scene.AddComponent<SceneChangeComponent>();
      Game.Scene.AddComponent<ClientNetworkComponent>();
      Game.Scene.AddComponent<MessageDispatcherComponent>();

      Game.Hotfix.GotoHotfix();

      Game.EventSystem.Run(EventIdType.LoadingEnd);
    }

    void Update()
    {
      Game.Hotfix.Update?.Invoke();
      Game.EventSystem.Update();
      context.Update();
    }
  }
}