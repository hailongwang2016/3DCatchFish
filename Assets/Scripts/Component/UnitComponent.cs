﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/1 15:13:05
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  [ObjectSystem]
  public class UnityComponentSystem : ObjectSystem<UnitComponent>, IAwake
  {
    public void Awake()
    {
      Self.Awake();
    }
  }

  public class UnitComponent : Component
  {
    public static UnitComponent Instance { get; private set; }

    public readonly Dictionary<long, Unit> idUnits = new Dictionary<long, Unit>();

    public void Awake()
    {
      Instance = this;
    }

    public override void Dispose()
    {
      if (this.Id == 0)
        return;
      base.Dispose();

      foreach (Unit unit in this.idUnits.Values)
      {
        unit.Dispose();
      }

      idUnits.Clear();

      Instance = null;
    }

    public void Add(Unit unit)
    {
      this.idUnits.Add(unit.Id, unit);
    }

    public Unit Get(long id)
    {
      Unit unit;
      this.idUnits.TryGetValue(id, out unit);
      return unit;
    }

    public void Remove(long id)
    {
      Unit unit;
      this.idUnits.TryGetValue(id, out unit);
      this.idUnits.Remove(id);
      unit?.Dispose();
    }

    public void RemoveNoDispose(long id)
    {
      this.idUnits.Remove(id);
    }

    public int Count
    {
      get
      {
        return this.idUnits.Count;
      }
    }

    public Unit[] GetAll()
    {
      return this.idUnits.Values.ToArray();
    }
  }
}