﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/1 13:11:26
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ETSModel
{
  [ObjectSystem]
  public class SceneChangeComponentSystem : ObjectSystem<SceneChangeComponent>, IUpdate
  {
    public void Update()
    {
      Self.Update();
    }
  }

  /// <summary>
  /// 场景加载组件
  /// </summary>
  public class SceneChangeComponent : Component
  {
    private AsyncOperation ao;
    private TaskCompletionSource<bool> tcs;
    public float Process;
    public Task<bool> ChangeSceneAsync(SceneType sceneType)
    {
      ao = SceneManager.LoadSceneAsync(sceneType.ToString());
      tcs = new TaskCompletionSource<bool>();
      return tcs.Task;
    }

    public void Update()
    {
      if (ao == null) return;

      this.Process = ao.progress;
      if (ao.isDone)
      {
        tcs.SetResult(true);
        ao = null;
      }
    }
  }
}