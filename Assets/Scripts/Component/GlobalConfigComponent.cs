﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 11:31:38
*   描述说明：
*****************************************************************/

namespace ETSModel
{
  [ObjectSystem]
  public class GlobalConfigComponentSystem : ObjectSystem<GlobalConfigComponent>, IAwake<string>
  {
    public void Awake(string str)
    {
      Self.Awake(str);
    }
  }

  public class GlobalConfigComponent : Component
  {
    public static GlobalConfigComponent Instance;
    public GlobalProto GlobalProto;
    public void Awake(string str)
    {
      Instance = this;
      GlobalProto = JsonHelper.FromJson<GlobalProto>(str);
    }
  }
}