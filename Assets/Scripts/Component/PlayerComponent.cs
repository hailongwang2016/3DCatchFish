﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/2 11:58:02
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  [ObjectSystem]
  public class PlayerComponentSystem : ObjectSystem<PlayerComponent>, IAwake
  {
    public void Awake()
    {
      Self.Awake();
    }
  }

  public class PlayerComponent : Component
  {
    public static PlayerComponent Instance { get; private set; }

    public readonly Dictionary<long, Player> idPlayers = new Dictionary<long, Player>();

    public void Awake()
    {
      Instance = this;
    }

    public override void Dispose()
    {
      if (this.Id == 0)
        return;
      base.Dispose();

      foreach (Player player in this.idPlayers.Values)
      {
        player.Dispose();
      }

      idPlayers.Clear();

      Instance = null;
    }

    public void Add(Player player)
    {
      this.idPlayers.Add(player.Id, player);
    }

    public Player Get(long id)
    {
      Player player;
      this.idPlayers.TryGetValue(id, out player);
      return player;
    }

    public void Remove(long id)
    {
      Player player;
      this.idPlayers.TryGetValue(id, out player);
      this.idPlayers.Remove(id);
      player?.Dispose();
    }

    public void RemoveNoDispose(long id)
    {
      this.idPlayers.Remove(id);
    }

    public int Count
    {
      get
      {
        return this.idPlayers.Count;
      }
    }

    public Player[] GetAll()
    {
      return this.idPlayers.Values.ToArray();
    }
  }
}
