﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 11:30:38
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public class GlobalProto
  {
    public string AssetBundleServerUrl;
    public string Address;

    public string GetUrl()
    {
      string url = this.AssetBundleServerUrl;
#if UNITY_ANDROID
			url += "Android/";
#elif UNITY_IOS
			url += "IOS/";
#else
      url += "PC/";
#endif
      return url;
    }
  }
}
