﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/1 14:53:36
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETSModel
{
  public static class UIHelper
  {
    public static ReferenceCollection GetReferenceCollection(this UI ui)
    {
      return ui.GameObject.GetComponent<ReferenceCollection>();
    }

    /// <summary>
    /// 注入View
    /// </summary>
    /// <param name="ui"></param>
    public static void InjectView(this UI ui, Component component)
    {
      ReferenceCollection rc = ui.GetReferenceCollection();

      Type type = component.GetType();
      Log.Info(type.Name);
      PropertyInfo[] infos = type.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.SetProperty | BindingFlags.Static | BindingFlags.Instance);
      foreach (PropertyInfo prop in infos)
      {
        var attrs = prop.GetCustomAttributes(typeof(ViewAttribute), false);

        if (attrs.Length == 0) continue;

        var viewAttribute = (ViewAttribute)attrs[0];

        string key = prop.Name;
        if (!string.IsNullOrEmpty(viewAttribute.Key))
          key = viewAttribute.Key;

        GameObject go = rc.Get<GameObject>(key);

        if (prop.PropertyType == typeof(GameObject))
        {
          prop.SetValue(component, go, null);
        }
        else
        {
          var comp = go.GetComponent(prop.PropertyType);
          prop.SetValue(component, comp, null);
        }
      }
    }
  }
}