﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 20:53:25
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETSModel
{
  public static class GameObjectHelper
  {
    public static T Get<T>(this GameObject gameObject, string key) where T : class
    {
      try
      {
        return gameObject.GetComponent<ReferenceCollection>().Get<T>(key);
      }
      catch (Exception e)
      {
        throw new Exception($"获取{gameObject.name}的ReferenceCollector key失败, key: {key}", e);
      }
    }
  }
}
