﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 22:25:51
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public static class ILHelper
  {
    public static void InitILRuntime(ILRuntime.Runtime.Enviorment.AppDomain appDomain)
    {
      // 注册重定向函数

      // 注册委托
      appDomain.DelegateManager.RegisterMethodDelegate<AChannel, System.Net.Sockets.SocketError>();

      // 注册适配器
    }
  }
}