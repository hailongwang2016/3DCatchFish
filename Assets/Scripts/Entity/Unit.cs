﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/1 15:09:15
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETSModel
{
  /// <summary>
  /// 单位实体
  /// </summary>
  public class Unit : Entity
  {
    public GameObject GameObject { get; set; }
  }
}
