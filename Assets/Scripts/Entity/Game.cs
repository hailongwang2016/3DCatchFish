﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/9 20:45:05
*   描述说明：
*****************************************************************/
namespace ETSModel
{
  public static class Game
  {
    private static Scene scene;
    public static Scene Scene
    {
      get
      {
        return scene ?? (scene = new Scene());
      }
    }

    private static EventSystem eventSystem;
    public static EventSystem EventSystem
    {
      get
      {
        return eventSystem ?? (eventSystem = new EventSystem());
      }
    }

    private static ObjectPool objectPool;
    public static ObjectPool ObjectPool
    {
      get
      {
        return objectPool ?? (objectPool = new ObjectPool());
      }
    }

    private static Hotfix hotfix;
    public static Hotfix Hotfix
    {
      get
      {
        return hotfix ?? (hotfix = new Hotfix());
      }
    }
  }
}