﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/1 15:11:18
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  /// <summary>
  /// 玩家实体
  /// </summary>
  public class Player : Entity
  {
    /// <summary>
    /// 分数
    /// </summary>
    public int Score { get; set; }
  }
}