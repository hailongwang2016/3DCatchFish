﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/1 11:56:43
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public enum SceneType
  {
    Launcher,
    Game
  }

  public class Scene : Entity
  {
    public string Name { get; set; }

    public Scene()
    {
    }

    public Scene(long id) : base(id)
    {
    }

    public override void Dispose()
    {
      if (this.Id == 0)
      {
        return;
      }

      base.Dispose();
    }
  }
}