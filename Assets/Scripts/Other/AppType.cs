﻿/****************************************************************
*   作者：corer
*   创建时间：2018/2/5 20:35:59
*   描述说明：
*****************************************************************/

using System;
namespace ETSModel
{
  [Flags]
  public enum AppType
  {
    None = 0,
    /// <summary>
    /// 服务器管理
    /// </summary>
    Manager = 1,
    /// <summary>
    /// 代理服
    /// </summary>
    Gate = 1 << 1,
    /// <summary>
    /// 游戏服
    /// </summary>
    Game = 1 << 2,
    /// <summary>
    /// 数据库服
    /// </summary>
    DB = 1 << 3,
    /// <summary>
    /// 各类服务器和Actor位置服
    /// </summary>
    Location = 1 << 4,
    /// <summary>
    /// 所有服一起
    /// </summary>
    All = Gate | Game | DB | Manager | Location,
  }
}