﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 22:17:31
*   描述说明：
*****************************************************************/
using ILRuntime.CLR.Method;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public class ILStaticMethod : IStaticMethod
  {
    private readonly ILRuntime.Runtime.Enviorment.AppDomain appDomain;
    private readonly IMethod method;

    public ILStaticMethod(ILRuntime.Runtime.Enviorment.AppDomain appDomain, string typeName, string methodName, int paramsCount)
    {
      this.appDomain = appDomain;
      this.method = appDomain.GetType(typeName).GetMethod(methodName, paramsCount);
    }

    public override void Run()
    {
      this.appDomain.Invoke(this.method, null);
    }
  }
}