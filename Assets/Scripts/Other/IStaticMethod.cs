﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 21:16:49
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public abstract class IStaticMethod
  {
    public abstract void Run();
  }
}