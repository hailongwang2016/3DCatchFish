﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 21:19:04
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ETSModel
{
  public class MonoStaticMethod : IStaticMethod
  {
    private readonly MethodInfo methodInfo;

    public MonoStaticMethod(Type type, string methodName)
    {
      this.methodInfo = type.GetMethod(methodName);
    }

    public override void Run()
    {
      this.methodInfo.Invoke(null, null);
    }
  }
}