﻿/****************************************************************
*   作者：Morain
*   创建时间：2018/2/27 19:36:00
*   描述说明：
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
* Author：FONG
* Create Date：2017/10/3 3:13:00
* Version： 1.0
* Description：引用集合
*/
using UnityEngine;
using Object = UnityEngine.Object;

namespace ETSModel
{
  [Serializable]
  public class ReferenceCollectionData
  {
    public string key;
    public Object gameObject;
  }

  public class ReferenceCollection : MonoBehaviour, ISerializationCallbackReceiver
  {
    public List<ReferenceCollectionData> list = new List<ReferenceCollectionData>();

    private Dictionary<string, Object> dictionary = new Dictionary<string, Object>();

    public void OnBeforeSerialize()
    {

    }

    public void OnAfterDeserialize()
    {
      foreach (ReferenceCollectionData data in list)
      {
        dictionary[data.key] = data.gameObject;
      }
    }

    public T Get<T>(string key) where T : class
    {
      Object dictGo;
      if (!this.dictionary.TryGetValue(key, out dictGo))
      {
        return null;
      }
      return dictGo as T;
    }
  }
}