﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/6 14:10:44
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace ETSEditor
{
  public class BuildEditor : EditorWindow
  {

    [MenuItem("Tools/打包工具")]
    public static void ShowWindow()
    {
      GetWindow<BuildEditor>(false, "打包工具");
    }

    private void OnGUI()
    {
      if (GUILayout.Button("标记"))
      {
        SetAssetBundlePackingTag();
      }
    }

    /// <summary>
    /// 设置AssetBundle包名
    /// </summary>
    private void SetAssetBundlePackingTag()
    {

    }

    /// <summary>
    /// 清空AssetBundle包名
    /// </summary>
    private void ClearAssetBundlePackingTag()
    {

    }
  }
}