﻿/****************************************************************
*   作者：corer
*   创建时间：2018/3/6 14:20:27
*   描述说明：
*****************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETSEditor
{
  public static class EditorResHelper
  {
    /// <summary>
    /// 获取文件内所有资源路径
    /// </summary>
    /// <param name="srcPath"></param>
    /// <param name="subDir"></param>
    /// <returns></returns>
    public static List<string> GetAllResourcePath(string srcPath, bool subDir)
    {
      List<string> paths = new List<string>();

      string[] files = Directory.GetFiles(srcPath);

      foreach (string item in files)
      {
        if (item.EndsWith(".meta")) continue;
        paths.Add(item);
      }

      if (subDir)
      {
        foreach (string subPath in Directory.GetDirectories(srcPath))
        {
          List<string> subFiles = GetAllResourcePath(srcPath, true);
          paths.AddRange(subFiles);
        }
      }
      return paths;
    }
  }
}
