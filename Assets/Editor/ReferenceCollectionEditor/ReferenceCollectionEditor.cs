﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ETSModel
{
  [CustomEditor(typeof(ReferenceCollection))]
  public class ReferenceCollectionEditor : Editor
  {
    private ReferenceCollection rc;
    private Object source;
    private void Awake()
    {
      rc = (base.target as ReferenceCollection);
    }

    public override void OnInspectorGUI()
    {
      // base.OnInspectorGUI();

      GUILayout.BeginHorizontal();
      if (GUILayout.Button("添加", GUILayout.MaxWidth(80f), GUILayout.MinHeight(24f)))
      {
        rc.list.Add(new ReferenceCollectionData());
      }

      if (GUILayout.Button("移除所有", GUILayout.MaxWidth(80f), GUILayout.MinHeight(24f)))
      {
        rc.list.Clear();
      }

      GUILayout.EndHorizontal();

      for (int i = 0; i < rc.list.Count; i++)
      {
        var item = rc.list[i];

        GUILayout.BeginHorizontal();
        GUILayout.Label("key:");
        EditorGUILayout.TextField(item.key);
        item.gameObject = EditorGUILayout.ObjectField(item.gameObject, typeof(Object), true);
        if (item.gameObject != null)
        {
          item.key = item.gameObject.name;
        }
        if (GUILayout.Button("x", GUILayout.MaxWidth(20f)))
        {
          rc.list.Remove(item);
        }
        GUILayout.EndHorizontal();
      }
    }
  }
}